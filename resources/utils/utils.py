import urlresolver


class Utils:
    serie = {}

    def get_key(self, item):
        self.is_not_used()
        if item['rapidvideo']:
            return item['key'], 'rapidvideo'
        elif item['streamango']:
            return item['key'], 'streamango'
        elif item['openload']:
            return item['key'], 'openload'

    def get_resolved(self, server, key):
        self.is_not_used()
        if server == 'rapidvideo':
            return self.resolve('https://www.rapidvideo.com/embed/' + key)
        elif server == 'streamango':
            return self.resolve('https://streamango.com/embed/' + key)
        elif server == 'openload':
            return self.resolve('https://openload.co/embed/' + key)

    def resolve(self, url):
        self.is_not_used()
        stream_url = urlresolver.HostedMediaFile(url=url).resolve()
        if not stream_url:
            return False
        else:
            return stream_url

    def is_not_used(self):
        pass
